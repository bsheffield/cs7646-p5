"""MC2-P1: Market simulator.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Atlanta, Georgia 30332  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
All Rights Reserved  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Template code for CS 4646/7646  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
works, including solutions to the projects assigned in this course. Students  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
and other users of this template code are advised not to share it with others  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or to make it available on publicly viewable websites including repositories  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
such as github and gitlab.  This copyright statement should not be removed  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or edited.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
We do grant permission to share solutions privately with non-students such  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
as potential employers. However, sharing with other current or future  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
GT honor code violation.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
-----do not edit anything above this line---  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""

import pandas as pd
import numpy as np
from util import get_data, plot_data


def compute_portvals(orders_file="./orders/orders.csv", start_val=1000000, commission=9.95, impact=0.005):
    """
    # this is the function the autograder will call to test your code
    # NOTE: orders_file may be a string, or it may be a file object. Your
    # code should work correctly with either input
    :param orders_file: is the name of a file from which to read orders
    :param start_val: is the starting value of the portfolio (initial cash available)
    :param commission: is the fixed amount in dollars charged for each transaction (both entry and exit)
    :param impact: is the amount the price moves against the trader compared to the historical data at each transaction
    :return:
    """

    # read in adjusted closing prices for the equities
    order_book_df = pd.read_csv(orders_file, index_col="Date", parse_dates=True, na_values=['nan'])

    start_date = order_book_df.index.values[0]
    end_date = order_book_df.index.values[-1]
    dates = pd.date_range(start_date, end_date)

    # obtain stock symbols without duplicates
    symbols = set(order_book_df["Symbol"].to_list())

    stock_prices_df = get_data(symbols, dates, addSPY=True)
    stock_prices_df["Cash"] = np.ones(stock_prices_df.shape[0])

    cash_balance_df = stock_prices_df * 0.0
    cash_balance_df.iloc[0, -1] = start_val

    for date_index, order in order_book_df.iterrows():
        stock_name = order[0]
        order_price = stock_prices_df[stock_name].loc[date_index]
        order_units = order[2]
        coefficient = -1 if order[1] == "BUY" else 1

        cash_balance_df.loc[date_index, stock_name] += order_units * coefficient * -1
        market_impact = order_units * order_price * impact
        cash_balance_df.loc[date_index, "Cash"] += order_units * order_price * coefficient - commission - market_impact

    portfolio_df = stock_prices_df * update_cash_balance(cash_balance_df)

    return total_daily_returns(portfolio_df)


def update_cash_balance(cash_balance_df):
    """
    Iteratively updates the cash balance with the new
    :param cash_balance_df:
    :return:
    """

    for i in range(1, cash_balance_df.shape[0]):
        for j in range(0, cash_balance_df.shape[1]):
            cash_balance_df.iloc[i, j] = cash_balance_df.iloc[i, j] + cash_balance_df.iloc[i - 1, j]

    return cash_balance_df


def total_daily_returns(portfolio_df):
    """Calculates the portfolio value based on total amount of daily returns"""

    portfolio_df["value"] = portfolio_df.sum(axis=1)
    portfolio_df["daily_returns"] = portfolio_df["value"][1:] / portfolio_df["value"][:-1].values - 1
    portfolio_df["daily_returns"][0] = 0

    return portfolio_df.iloc[:, -2:-1]


def compute_portfolio_stats(portfolio_df, rfr=0.0, sf=252.0, sv=10000):
    """
    Returns common portfolio statistics based on current portfolio.
    :param portfolio_df: Dataframe
    :param rfr: Risk free return assumed to be 0
    :param sf: Number of days traded assumed to be 252 days out of the year.
    :return: avg_daily_returns, sharpe_ratio

    """

    daily_rets = (portfolio_df / portfolio_df.shift(1)) - 1
    daily_rets = daily_rets[1:]
    cumulative_ret = (portfolio_df.iloc[-1] - sv) / sv
    avg_daily_ret = daily_rets.mean()
    std_daily_ret = daily_rets.std()
    sharpe_ratio = np.sqrt(252) * daily_rets.mean() / std_daily_ret

    return cumulative_ret.value, avg_daily_ret.value, std_daily_ret.value, sharpe_ratio.value

def test_code():
    # this is a helper function you can use to test your code  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # note that during autograding his function will not be called.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    # Define input parameters  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			

    of = "./orders/orders-02.csv"
    sv = 1000000

    # Process orders  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    portvals = compute_portvals(orders_file=of, start_val=sv)
    cr, adr, sddr, sr = compute_portfolio_stats(portvals)
    print("Cumulative Returns: " + str(cr))
    print("Average Daily Returns: " + str(adr))
    print("Standard Deviation of Returns: " + str(sddr))
    print("Sharpe Ratio: " + str(sr))


def author():
    return 'bsheffield7'


if __name__ == "__main__":
    test_code()
